#warning


|====== Modified by Clyde for EZ Server to fit my cMacro echo system
|====== Original credit: bruise - http://ezserver.online/forums/index.php?topic=6207.0
|====== You can contact me on Discord @ Clyde#1111
|====== Use at your own risk, let me know if you have any issues and I'll try to fix them.
|======
|======
|====== Clyde's Additions / Changes
|======	Added INI control instead of seperate macros per Toon/Zone/ETC, INI can do the same thing, but cleaner IMO
|====== Instead of a blanket loot some value, can set how many to loot in INI
|====== Added a destroy functionality



#include cMacros\cCommonUtils.inc


#Event cTurboLoot	"[MQ2] cTurboLoot #1#"


|==== Sub Event_cTurboLoot
	Sub Event_cTurboLoot(Line, cCharName)
		/delay 2s
		/call cTurboLootLogic ${cCharName}
	/return
|==== End Sub Event_cTurboLoot



Sub cTurboLootLogic(cCharName)
	
	/if (!${Defined[dummy]}) /declare dummy int local
	/if (!${Defined[turbolootitems]}) /declare turbolootitems	string	outer
	/if (!${Defined[AlertIndex]}) /declare AlertIndex int outer 1
	/if (!${Defined[loottry]}) /declare loottry int outer 0
	/if (!${Defined[Skip]}) /declare Skip bool outer FALSE
	/if (!${Defined[minLootValue]}) /declare minLootValue int outer
	/if (!${Defined[loottotal]}) /declare loottotal int outer 
	/if (!${Defined[sometotal]}) /declare sometotal int outer
	/if (!${Defined[singletotal]}) /declare singletotal int outer
	/if (!${Defined[destroytotal]}) /declare destroytotal int outer
	/if (!${Defined[lootslot]}) /declare lootslot int outer 
	/if (!${Defined[someslot]}) /declare someslot int outer
	/if (!${Defined[singleslot]}) /declare singleslot int outer
	/if (!${Defined[destroyslot]}) /declare destroyslot int outer
	/if (!${Defined[lootAll]}) /declare lootAll string outer
	/if (!${Defined[someItems]}) /declare someItems string outer
	/if (!${Defined[singleItems]}) /declare singleItems string outer
	/if (!${Defined[destroyItems]}) /declare destroyItems string outer
	/if (!${Defined[announceItems]}) /declare announceItems string outer
	/if (!${Defined[lootName]}) /declare lootName string outer
	/if (!${Defined[lootSome]}) /declare lootSome string outer
	/if (!${Defined[lootSingle]}) /declare lootSingle string outer
	/if (!${Defined[destroyName]}) /declare destroyName string outer
	/if (!${Defined[itemCount]}) /declare itemCount int outer
	/if (!${Defined[someCount]}) /declare someCount int outer
	/if (!${Defined[singleCount]}) /declare singleCount int outer
	/if (!${Defined[destroyCount]}) /declare destroyCount int outer
	/if (!${Defined[cTurboLootIni]}) /declare cTurboLootIni string outer cMacros\cTurboLootIni\cTurboLoot_${cCharName}.ini
	
	
	/if (!${Defined[lootUsable]}) /declare lootUsable int outer 0	|Set this to 1 if you want to loot an item that you don't currently have, isn't in any of the 3 lists below but is usable and can be equipped.
	/varset minLootValue 999999999 | Will loot any item with a vendor price >= this amount (in plat).

	| lootAll list will loot this item any time it comes across it on a corpse. 
	| someItems will loot x amount based on number set in the INI
	| singleItems will loot until you have 1 in total. Combines bank + inventory when calculating.	
	/varset lootAll ${Ini[${cTurboLootIni},LootAll]}
	/varset someItems ${Ini[${cTurboLootIni},SomeItems]}
	/varset singleItems ${Ini[${cTurboLootIni},SingleItems]}	
	/varset destroyItems ${Ini[${cTurboLootIni},Destroy]}
	/varset announceItems ${Ini[${cMacIni},AnnounceLoot]}

	/call openBags
	
	/hidecorpse looted
	/say #corpsefix
	/echo [quickLoot] ${Time} Started Looting

    
    :mainlootloop
    /delay 1
	/if (${cNumOpenSlots[PlaceHolder]}) {
		/if (${SpawnCount[npccorpse radius 100 noalert 1]} > 0) {
			/target npccorpse${If[${AlertIndex}, noalert ${AlertIndex},]}

			/if ((${Target.ID}) && (${Target.Type.Equal[Corpse]})) {
					/face fast
					:moveToCorpse
						/if (${Target.Distance} > 10) {
							/moveto id ${Target.ID}
							/delay 5
							/while (${Window[LootWnd].Open}) {
								/notify LootWnd DoneButton leftmouseup
								/delay 1
								/notify LootWnd DoneButton leftmouseup
							}
							/goto :moveToCorpse
						}

				/if (${Cursor.ID}) /autoi
				/loot
				/doevents

				/varset loottotal ${Corpse.Items}
				/varset sometotal ${Corpse.Items}
				/varset singletotal ${Corpse.Items}
				/varset destroytotal ${Corpse.Items}
				/if (${loottotal}<=0 && ${sometotal}<=0 && ${singletotal} <=0 && ${destroytotal} <= 0) { 
					/while (${Window[LootWnd].Open}) {
						/notify LootWnd DoneButton leftmouseup
						/delay 1
						/notify LootWnd DoneButton leftmouseup
					}					
					/goto :mainlootloop 
				} 
				
				

				/varset itemCount 0
				/for lootslot 1 to ${loottotal}
					/if (${Corpse.Item[${lootslot}].ID}) {
						/varset lootName ${Corpse.Item[${lootslot}].Name}	
						/delay 1
						
						/if (${announceItems.Find[|${lootName}|]}) {
							/docommand /docommand /${AnnounceChannel} ${Corpse.Item[${lootslot}].ItemLink[CLICKABLE]} found on ID - ${Target.ID}
						}
					/if ((${lootAll.Find[|${lootName}|]}) || ((${Corpse.Item[${lootslot}].Value} > ${Math.Calc[${minLootValue} * 1000]}) && (!${Corpse.Item[${lootslot}].NoDrop}))) {
							/varcalc itemCount ${itemCount}+1
					} else {
						/echo ${Corpse.Item[${lootslot}].Name} found on ${Target.ID}
					}
					}
				/next lootslot
				/doevents

				/for lootslot 1 to ${loottotal}
					/if ((${Corpse.Item[${lootslot}].CanUse}) && (${Corpse.Item[${lootslot}].WornSlots}>0)) {
						/if ((${Math.Calc[${FindItemCount[${Corpse.Item[=${lootslot}]}]}+${FindItemBankCount[=${lootslot}]}]}<1) && ${lootUsable}==1) {
						/echo [Usable] Looting ${Corpse.Item[${lootslot}].ItemLink[CLICKABLE]}
						/while (${Corpse.Item[${lootslot}].ID}) {
							/ctrl /itemnotify loot${intLootSlot} rightmouseup
						}
						}
					}
				/next lootslot


				/varset someCount 0
				/for someslot 1 to ${sometotal}
					/if (${Corpse.Item[${someslot}].ID}) {
						/varset lootSome ${Corpse.Item[${someslot}].Name}						
						/if (${someItems.Find[|${lootSome}|]}) {
							/varcalc someCount ${someCount}+1
						}
					}
				/next someslot
				/doevents

				/varset singleCount 0
				/for singleslot 1 to ${singletotal}
					/if (${Corpse.Item[${singleslot}].ID}) {
						/varset lootSingle ${Corpse.Item[${singleslot}].Name}						
						/if (${singleItems.Find[|${lootSingle}|]}) {
							/varcalc singleCount ${singleCount}+1
						}
					}
				/next singleslot
				/doevents
				
				/varset destroyCount 0
				/for destroyslot 1 to ${destroytotal}
					/if (${Corpse.Item[${destroyslot}].ID}) {
						/varset destroyName ${Corpse.Item[${destroyslot}].Name}						
						/if (${destroyItems.Find[|${destroyName}|]}) {
							/varcalc destroyCount ${destroyCount}+1
						}
					}
				/next destroyslot
				/doevents

				/if (${itemCount} > 0) {
					/for lootslot 1 to ${loottotal}
						:LootItem
						/if (${Corpse.Item[${lootslot}].ID}) {
							/varset lootName ${Corpse.Item[${lootslot}].Name}
																																
							/if ((${lootAll.Find[|${lootName}|]}) || ((${Corpse.Item[${lootslot}].Value} > ${Math.Calc[${minLootValue} * 1000]}) && (!${Corpse.Item[${lootslot}].NoDrop}))) /call lootItem ${lootslot}						
						}

					/next lootslot
					/doevents
				}
					/doevents
				/if (${someCount} > 0) {
					/for someslot 1 to ${sometotal}
						:LootSomeItems
						/if (${Corpse.Item[${someslot}].ID}) {
							/varset lootSome ${Corpse.Item[${someslot}].Name}
																																
							/if (${someItems.Find[|${lootSome}|]}) /call someItem ${someslot}						
						}

					/next someslot
					/doevents
				}

				/if (${singleCount} > 0) {
					/for singleslot 1 to ${singletotal}
						:LootSingleItem
						/if (${Corpse.Item[${singleslot}].ID}) {
							/varset lootSingle ${Corpse.Item[${singleslot}].Name}
																																
							/if (${singleItems.Find[|${lootSingle}|]}) /call singleItem ${singleslot}						
						}

					/next singleslot
					/doevents
				}
				
				/if (${destroyCount} > 0) {
					/for destroyslot 1 to ${destroytotal}
						:DestroyItem
						/if (${Corpse.Item[${destroyslot}].ID}) {
							/varset destroyName ${Corpse.Item[${destroyslot}].Name}
																																
							/if (${destroyItems.Find[|${destroyName}|]}) /call destroyItem ${destroyslot}						
						}

					/next destroyslot
					/doevents
				}

				
				:DoneLooting
				|/echo DoneLooting
				/while (${Window[LootWnd].Open}) {
					/notify LootWnd DoneButton leftmouseup
					/delay 1
					/notify LootWnd DoneButton leftmouseup
				}
				/if (${loottry} > 0) /varset loottry 0
				
				/doevents
				/if (${Window[LootWnd].Open}) /goto :DoneLooting
			}	
		} else {
			/if (${Me.Grouped}) /echo \arDone Looting
			/if (${AlertIndex}) /alert clear ${AlertIndex}
			/if (${loottry} > 0) /varset loottry 0 
			/call closeBags

|			/beep
			/return
		}
	} else {
		/echo [quickLoot] \arMy Inventory is full!!
		/return
	}
	/goto :mainlootloop
/return

| ----------------------------------------------------------------------------------------------------------

sub lootItem(intLootSlot)

	|If item is LORE but already have one, don't loot it
	|/if (${Corpse.Item[${intLootSlot}].Lore}) {
	|	/echo on me: ${FindItemCount[=${lootName}]}
	|	/echo bank: ${FindItemBankCount[=${lootName}]}
	|	/echo BIG: ${Int[${Math.Calc[${FindItemCount[=${lootName}]}+${FindItemBankCount[=${lootName}]}]}]}
	|	/if (${Int[${Math.Calc[${FindItemCount[=${lootName}]}+${FindItemBankCount[=${lootName}]}]}]}>=1)
	|	/echo [lootLORE] Skipping LORE ${Corpse.Item[${intLootSlot}].ItemLink[CLICKABLE]} - Already have
	|	
	|}
	/delay 1
	/echo [lootALL] \agLooting \ap${Corpse.Item[${intLootSlot}].Name}
	/while (${Corpse.Item[${intLootSlot}].ID}) {
		/delay 1
		/ctrl /itemnotify loot${intLootSlot} rightmouseup
		/delay 1
	}
	
/return


| ----------------------------------------------------------------------------------------------------------


sub someItem(intSomeSlot)

	/if (!${Defined[howMany]}) /declare howMany int outer ${Ini[${cTurboLootIni},SomeItems,${Corpse.Item[${intSomeSlot}].Name}]}
	|/echo Slot: ${intSomeSlot}
	|/echo INI: ${cTurboLootIni}
	|/echo How Many ${howMany}
	|/echo Ini val: ${Ini[${cTurboLootIni},SomeItems,${Corpse.Item[${intSomeSlot}].Name}]}
	|/echo We Have: ${Int[${Math.Calc[${FindItemCount[=${lootSome}]}+${FindItemBankCount[=${lootSome}]}]}]}

	| Loot until X amount in inventory or bank
	/if (${Corpse.Item[${intSomeSlot}].ID}) {
		/if (${Int[${Math.Calc[${FindItemCount[=${lootName}]}+${FindItemBankCount[=${lootName}]}]}]} >= ${Ini[${cTurboLootIni},SomeItems,${Corpse.Item[${intSomeSlot}].Name}]}) {
		/echo \ay[lootSOME] Already have enough ${Corpse.Item[=${intSomeSlot}].ItemLink[CLICKABLE]} - IGNORED			
		/return
	}
	
	/echo [lootSOME] \agLooting \ap${Corpse.Item[${intSomeSlot}].ItemLink[CLICKABLE]} - Total: ${Int[${Math.Calc[${FindItemCount[=${lootName}]}+1+${FindItemBankCount[=${lootName}]}]}]}
	/while (${Corpse.Item[${intSomeSlot}].ID}) {
		/ctrl /itemnotify loot${intSomeSlot} rightmouseup
	}

	}
/return


| ----------------------------------------------------------------------------------------------------------

sub singleItem(intSingleSlot)

	| Loot until I have 1 in inventory or bank
	/if (${Corpse.Item[${intSingleSlot}].ID}) {
		/if (${Int[${Math.Calc[${FindItemCount[=${lootName}]}+${FindItemBankCount[=${lootName}]}]}]}>=1) {			
			/echo \ay[lootSINGLE] Skipping ${Corpse.Item[${intSingleSlot}].ItemLink[CLICKABLE]} - IGNORED			
			/return
		}
	}
	/echo [lootSINGLE] \agLooting \ap${Corpse.Item[${intSingleSlot}].ItemLink[CLICKABLE]}
	/while (${Corpse.Item[${intSingleSlot}].ID}) {
		/ctrl /itemnotify loot${intSingleSlot} rightmouseup
	}
	
/return


| ----------------------------------------------------------------------------------------------------------


sub destroyItem(intLootSlot)

	| If item is LORE but already have one, don't loot it
	/if (${Corpse.Item[${intLootSlot}].Lore}) {
		/if (${Int[${Math.Calc[${FindItemCount[=${lootName}]}+${FindItemBankCount[=${lootName}]}]}]}>=1) 
		/echo \ay[lootLORE] Skipping LORE ${Corpse.Item[${intLootSlot}].ItemLink[CLICKABLE]} - Already have
		
	}
	/echo [DESTROY] Looting \ar${Corpse.Item[${intLootSlot}].ItemLink[CLICKABLE]}
	:WaitDestroyItem
		/autoinv
		/autoinv
		/delay 1
		/ctrl /itemnotify loot${intLootSlot} leftmouseup
		/delay 3		
		/destroy
		/autoinv
		
		/if (${Corpse.Item[${intLootSlot}].ID}) {
			/goto :WaitDestroyItem
		}
	
/return


| ----------------------------------------------------------------------------------------------------------

Sub openBags
	/if (!${Defined[bag]}) /declare bag int local
	
	/if (!${Window[InventoryWindow].Open}) /nomodkey /keypress inventory
	/for bag 1 to 10
		/if (!${Window[pack${bag}].Open}) /nomodkey /itemnotify pack${bag} rightmouseup
	/next bag

/return

| ----------------------------------------------------------------------------------------------------------

Sub closeBags
	/if (!${Defined[bag]}) /declare bag int local
	
	/if (${Window[InventoryWindow].Open}) /nomodkey /notify InventoryWindow DoneButton leftmouseup
	/for bag 1 to 10
		/if (${Window[pack${bag}].Open}) /nomodkey /itemnotify pack${bag} rightmouseup
	/next bag

/return

| ----------------------------------------------------------------------------------------------------------